#===============================================================================
# gene_dist.py
#===============================================================================

# Imports ======================================================================

from argparse import ArgumentParser
from pybedtools import BedTool
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os.path




# Constants ====================================================================

RESULTS_DIR = os.path.dirname(os.path.dirname(__file__))
GENE_COORD_PATH = os.path.join(RESULTS_DIR, 'genome_features',
                               'Lm7210.A02.genes.gff3')
CHROM_SIZES = pd.read_table(os.path.join(RESULTS_DIR, 'genome_features', 'Lm7210.A02.chrom.sizes'),
    names=('chrom', 'size'), index_col=0).squeeze('columns')

COLORS_DICT = {
    'Lm7210': sns.color_palette('bright')[0],
    'Sp9509': sns.color_palette('dark')[7],
    'Wa8730': sns.color_palette('dark')[9],
    'Wa8730': sns.color_palette('dark')[6]
}




# Functions ====================================================================

def parse_gff_attributes(attr):
    return dict(pair.split('=') for pair in attr.split(';'))


def parse_gff(gff: str, flank: int = 1000, type='gene'):
    with open(gff) as f:
        for l in f:
             if not l.startswith('##'):
                seqid, _, t, start, end, _, strand, _, attr = l.rstrip().split(
                                                                           '\t')
                if ((t == type) or (type is None)) and int(start) > flank:
                    yield (seqid, int(start), int(end), strand,
                           parse_gff_attributes(attr))


def map_genes_to_coords(genes):
    gene_to_coord =  pd.DataFrame(((attr['ID'], chrom, start, end, strand)
            for chrom, start, end, strand, attr in parse_gff(genes)),
        columns=('id', 'chrom', 'start', 'end', 'strand'))
    gene_to_coord.index = gene_to_coord['id']
    return gene_to_coord.loc[:, ('chrom', 'start', 'end', 'strand')]


def generate_gene_coords(gene_to_coord):
    for gene_name, row in gene_to_coord.iterrows():
        chrom, start, stop, strand = row
        yield chrom, int(start), int(stop), gene_name, '.', strand


def gene_coord_bedtool():
    gene_to_coord =  map_genes_to_coords(GENE_COORD_PATH)
    return BedTool(tuple(generate_gene_coords(gene_to_coord)))


def parse_arguments():
    parser = ArgumentParser(description='plot distribution of genes')
    parser.add_argument('chrom', metavar='<X>', nargs='+',
        help='chromosomes to plot')
    return parser.parse_args()


def main():
    sns.set_context('talk')
    args = parse_arguments()
    gene_to_coord =  map_genes_to_coords(GENE_COORD_PATH)
    gene_coords = BedTool(tuple(generate_gene_coords(gene_to_coord)))
    fig, axes = plt.subplots(1, len(args.chrom), squeeze=False, sharey=True, gridspec_kw={'width_ratios': [CHROM_SIZES[int(x)] for x in args.chrom]})
    fig.set_figheight(3)
    fig.set_figwidth(4*len(args.chrom))
    for ax in range(len(args.chrom)):
        genes_chrom = pd.DataFrame(((int(start), int(end)) for _, start, end, _, _, _ in gene_coords.intersect(
            BedTool(((str(args.chrom[ax]), 1, int(CHROM_SIZES[int(args.chrom[ax])])),))))).mean(axis=1) / 1e6
        sns.kdeplot(x=genes_chrom, fill=True, bw_adjust=.25,
            color=sns.color_palette('bright')[0], ax=axes[0, ax])
        # rounded_yticks = tuple(int(round(total_genes * dens, -1)) for dens in axes[0, ax].get_yticks()[:-1:2])
        # axes[0, ax].set_yticks(tuple(n / (total_genes) for n in rounded_yticks))
        # axes[0, ax].set_yticklabels(rounded_yticks)
        axes[0, ax].set_xlim((0, CHROM_SIZES[int(args.chrom[ax])]/1e6))
        axes[0, ax].set_xlabel(f'Chr {args.chrom[ax]} (MB)')
        axes[0, ax].set_ylabel('Genes per MB')
        total_genes = len(genes_chrom)
    rounded_yticks = tuple(int(round(total_genes * dens, -1)) for dens in plt.yticks()[0][:-1:2])
    plt.yticks(ticks=tuple(n / (total_genes) for n in rounded_yticks), labels=rounded_yticks)
    fig.tight_layout()
    fig.savefig(os.path.join(RESULTS_DIR, 'feature_distributions', 'gene_distributions',
        f'genes_dist.svg'))
    fig.savefig(os.path.join(RESULTS_DIR, 'feature_distributions', 'gene_distributions',
        f'genes_dist.pdf'))
    fig.clf()




# Execute ======================================================================

if __name__ == '__main__':
    main()
    