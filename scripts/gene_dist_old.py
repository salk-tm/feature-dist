#===============================================================================
# gene_dist.py
#===============================================================================

# TODO: check on validity of annotations...

# Imports ======================================================================

from argparse import ArgumentParser
from pybedtools import BedTool
from math import floor
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os.path
import gff2bed


# Constants ====================================================================

SIZES_BASENAME = {
    'Lm7210': 'Lm7210.A02.chrom.sizes',
    'Sp9509': 'sp9509.asm3v1.sizes',
    'Wa8730': 'wa8730.asm201904v2.sizes',
    'Lg7742a': sns.color_palette('dark')[6]
}

GENES_BASENAME = {
    'Lm7210': 'Lm7210.A02.genes.gff3',
    # 'Lm7210': 'Lm7210a.2022v1.genes.gff3.gz',
    'Sp9509': 'Sp9509d.2022v1.genes.gff3.gz',
    # 'Sp9509': 'Spirodela_polyrhiza_Greater_Duckweed_strain_9509_annos1-cds0-id_typename-nu1-upa1-add_chr0.gid51364.gff',
    'Wa8730': 'Wa8730c.2022v1.genes.gff3.gz',
    'Lg7742a': sns.color_palette('dark')[6]
}

REPEATS_BASENAME = {
    'Lm7210': 'Lm7210.A02.repeats.gff3',
    'Sp9509': 'sp9509.asm3v1.fasta.mod.EDTA.TEanno.gff',
    'Wa8730': 'wa8730.asm201904v2.fasta.mod.EDTA.TEanno.gff',
    'Lg7742a': sns.color_palette('dark')[6]
}

CHIPSEQ_BASENAME = {
    'Lm7210': 'lm7210.asm202106v1.chrs+organelles.poslm7210.neglm7210.macs_peaks.narrowPeak',
    'Sp9509': 'sp9509.asm3v1.possp.negsp.macs_peaks.narrowPeak',
    'Wa8730': 'wa8730.asm201904v2.poswa.negwa.macs_peaks.narrowPeak',
    'Lg7742a': sns.color_palette('dark')[6]
}

RESULTS_DIR = os.path.dirname(os.path.dirname(__file__))
COORD_DIR = os.path.join(RESULTS_DIR, 'genome_features_old')
CHROM_SIZES = pd.read_table(os.path.join(RESULTS_DIR, 'genome_features', 'Lm7210.A02.chrom.sizes'),
    names=('chrom', 'size'), index_col=0).squeeze('columns')

COLORS_DICT = {
    'Lm7210': sns.color_palette('bright')[0],
    'Sp9509': sns.color_palette('dark')[7],
    'Wa8730': sns.color_palette('dark')[9],
    'Lg7742a': sns.color_palette('dark')[6]
}




# Functions ====================================================================

def map_features_to_coords(features, type='gene'):
    gene_to_coord =  pd.DataFrame(((attr['ID'], chrom, start, end, strand)
            for chrom, start, end, strand, attr in gff2bed.parse(features, type=type, graceful=True) if 'ID' in attr.keys()),
        columns=('id', 'chrom', 'start', 'end', 'strand'))
    gene_to_coord.index = gene_to_coord['id']
    return gene_to_coord.loc[:, ('chrom', 'start', 'end', 'strand')]


def feature_coord_bedtool(coords, type='gene'):
    return BedTool(tuple(r for _, r in map_features_to_coords(coords, type=type).iterrows()))


def parse_arguments():
    parser = ArgumentParser(description='plot distribution of genes')
    parser.add_argument('chrom', metavar='<X>', nargs='+',
        help='chromosomes to plot')
    parser.add_argument('--line', metavar='<LINE>', choices=('Lm7210', 'Sp9509', 'Wa8730', 'Lg7742a'), 
        default='Sp9509', help='line to plot')
    return parser.parse_args()


def main():
    sns.set_context('paper')
    args = parse_arguments()
    if args.line in ('Lm7210', 'Wa8730'):
        args.chrom = [int(x) for x in args.chrom]
    chrom_sizes = pd.read_table(os.path.join(COORD_DIR, SIZES_BASENAME[args.line]), names=('chrom', 'size'), index_col=0).squeeze('columns')
    gene_coords = feature_coord_bedtool(os.path.join(COORD_DIR, GENES_BASENAME[args.line]))
    repeat_coords = feature_coord_bedtool(os.path.join(COORD_DIR, REPEATS_BASENAME[args.line]), type=None)
    chipseq_coords = BedTool(os.path.join(COORD_DIR, CHIPSEQ_BASENAME[args.line]))
    fig, axes = plt.subplots(3, len(args.chrom), squeeze=False, sharex='col', sharey='row', gridspec_kw={'width_ratios': [chrom_sizes[('chr'+str(x)) if args.line in ('Wa8730',) else x] for x in args.chrom]})
    fig.set_figheight(2)
    fig.set_figwidth(0.75*len(args.chrom))
    binwidth=0.5
    for ax in range(len(args.chrom)):
        chrom_size = int(chrom_sizes['chr'+str(args.chrom[ax]) if args.line in ('Wa8730',) else args.chrom[ax]])
        bins = [binwidth * x for x in range(floor(chrom_size / 1e6 / binwidth))] + [chrom_size / 1e6]
        genes_chrom = pd.DataFrame(((int(start), int(end)) for _, start, end, _ in gene_coords.intersect(
            BedTool(((('chr' if args.line in ('Wa8730', 'Sp9509') else '') + str(int(str(args.chrom[ax]).replace('Chr', '') if args.line=='Sp9509' else args.chrom[ax])), 1, chrom_size),))))).mean(axis=1) / 1e6
        sns.histplot(x=genes_chrom, bins=bins, element='step',
            color=COLORS_DICT[args.line], ax=axes[0, ax])
        axes[0, ax].set_xlim((0, chrom_size/1e6))
        # axes[0, ax].set_xlabel(f'Chr {args.chrom[ax]} (MB)')
        axes[0, ax].set_ylabel('Genes', rotation=0, horizontalalignment='right')
        if ax == 0:
            rounded_yticks = tuple(int(1/binwidth * x) for x in axes[0, ax].get_yticks())
            axes[0, ax].set_yticks(tuple(binwidth*x for x in rounded_yticks))
            axes[0, ax].set_yticklabels(rounded_yticks)

        repeats_chrom = pd.DataFrame(((int(start), int(end)) for _, start, end, _ in repeat_coords.intersect(
            BedTool(((('chr' if args.line in ('Wa8730') else '') + str(args.chrom[ax]), 1, chrom_size),))))).mean(axis=1) / 1e6
        sns.histplot(x=repeats_chrom, bins=bins, element='step',
            color=COLORS_DICT[args.line], ax=axes[1, ax])
        axes[1, ax].set_xlim((0, chrom_size/1e6))
        # axes[1, ax].set_xlabel(f'Chr {args.chrom[ax]} (MB)')
        axes[1, ax].set_ylabel('TEs', rotation=0, horizontalalignment='right')
        if ax == 0:
            rounded_yticks = tuple(int(1/binwidth * x) for x in axes[1, ax].get_yticks())
            axes[1, ax].set_yticks(tuple(binwidth * x for x in rounded_yticks))
            axes[1, ax].set_yticklabels(rounded_yticks)
        
        chipseq_chrom = pd.DataFrame(((int(start), int(end)) for _, start, end, *_ in chipseq_coords.intersect(
            BedTool(((('chr' if args.line in ('Lm7210', 'Wa8730') else '') + str(args.chrom[ax]), 1, chrom_size),))))).mean(axis=1) / 1e6
        sns.histplot(x=chipseq_chrom, bins=bins, element='step',
            color=COLORS_DICT[args.line], ax=axes[2, ax])
        axes[2, ax].set_xlim((0, chrom_size/1e6))
        axes[2, ax].set_xlabel(f"Chr {int(str(args.chrom[ax]).replace('Chr', ''))}", rotation=30, horizontalalignment='right')
        axes[2, ax].set_ylabel('CENH3', rotation=0, horizontalalignment='right')
        # if ax == 0:
        #     rounded_yticks = tuple(int(1/binwidth * x) for x in axes[2, ax].get_yticks())
        #     axes[2, ax].set_yticks(tuple(binwidth * x for x in rounded_yticks))
        #     axes[2, ax].set_yticklabels(rounded_yticks)

    fig.tight_layout(pad=0.2)
    fig.savefig(os.path.join(RESULTS_DIR, 'feature_distributions',
        f'features_dist_{args.line}.svg'))
    fig.savefig(os.path.join(RESULTS_DIR, 'feature_distributions',
        f'features_dist_{args.line}.pdf'))
    fig.clf()




# Execute ======================================================================

if __name__ == '__main__':
    main()
    